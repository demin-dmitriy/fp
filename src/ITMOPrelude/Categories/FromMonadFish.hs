{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module ITMOPrelude.Categories.FromMonadFish where
import ITMOPrelude.Primitive(id)
import ITMOPrelude.Categories.MonadFish

-- Эти
import ITMOPrelude.Categories hiding (id)
import ITMOPrelude.Categories.MonadJoin

-- делаем из нас
instance MonadFish m => Monad m where
    return = returnFish
    k >>= g = (>=>) id g k

instance MonadFish m => Functor m where
    -- Too easy!
    -- fmap = liftM
    -- Just for practice sake
    fmap f = id >=> (returnFish . f)

instance MonadFish m => MonadJoin m where
    returnJoin = returnFish
    join = id >=> id
