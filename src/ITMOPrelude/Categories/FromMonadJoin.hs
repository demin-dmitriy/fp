{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module ITMOPrelude.Categories.FromMonadJoin where
import ITMOPrelude.Categories.MonadJoin
import ITMOPrelude.Primitive((.))
-- Эти
import ITMOPrelude.Categories hiding ((.))
import ITMOPrelude.Categories.MonadFish

-- делаем из нас
instance MonadJoin m => Monad m where
    return = returnJoin
    k >>= g = join (fmap g k)

instance MonadJoin m => Functor m where
    fmap = liftM

instance MonadJoin m => MonadFish m where
    returnFish = returnJoin
    f >=> g = join . fmap g . join . fmap f . return
