{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, UndecidableInstances #-}
module ITMOPrelude.Categories.FromMonad where
import ITMOPrelude.Categories

-- Эти
import ITMOPrelude.Categories.MonadJoin
import ITMOPrelude.Categories.MonadFish

-- делаем из нас

instance Monad m => Functor m where
    fmap = liftM

instance Monad m => MonadJoin m where
    returnJoin = return
    join = (>>= id)

instance Monad m => MonadFish m where
    returnFish = return
    (>=>) f g x = f x >>= g
