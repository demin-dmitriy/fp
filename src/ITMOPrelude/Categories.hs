{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Categories where

-- Реализовать для всего,
-- что только можно из
import ITMOPrelude.Primitive hiding ((.), id)
import qualified ITMOPrelude.Primitive
import ITMOPrelude.List
import ITMOPrelude.Tree hiding (Nil)
-- всевозможные инстансы для классов ниже

--------------------------------------------------------------------------------
-- Классы
class Category cat where
    id  :: cat a a
    (.) :: cat b c -> cat a b -> cat a c

class Functor f where
    fmap :: (a -> b) -> f a -> f b

class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b

(>>) :: Monad m => m a -> m b -> m b
ma >> mb = ma >>= (\_ -> mb)

--------------------------------------------------------------------------------
-- Инстансы писать сюда

instance Category (->) where
    id x = x
    f . g = \x -> f (g x)

liftM :: Monad m => (a -> b) -> m a -> m b
liftM f x = x >>= (return . f)

-- Could use UndecidableInstances instead to make
-- instance Monad m => Functor where fmap = liftM 
instance Functor Maybe where
    fmap = liftM

instance Functor List where
    fmap = liftM

instance Functor Tree where
    fmap = mapTree

instance Monad Maybe where
    return x = Just x
    Nothing >>= _ = Nothing
    Just x >>= f = f x

instance Monad List where
    return x = Cons x Nil
    l >>= f = concatMap f l

--------------------------------------------------------------------------------
-- Монада State

newtype State s a = State { runState :: s -> (s, a) }

instance Monad (State s) where
    return x = State (\y -> (y, x))
    k >>= f = State mkState where
        mkState s0 = (s2, b) where
            (s1, a) = runState k s0
            (s2, b) = runState (f a) s1
