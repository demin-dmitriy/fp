{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Algebra where

-- Реализовать для всего,
-- что только можно из
import ITMOPrelude.Primitive
-- всевозможные инстансы для классов ниже 

-- Если не страшно, то реализуйте их и для
import ITMOPrelude.List
import ITMOPrelude.Tree

-- Классы
class Monoid a where
    mempty :: a
    mappend :: a -> a -> a

class Monoid a => Group a where
    ginv :: a -> a

-- Инстансы писать сюда
instance Monoid Unit where
    mempty = Unit
    mappend _ _ = Unit

instance Group Unit where
    ginv _ = Unit

instance Monoid Bool where
    mempty = True
    mappend = (&&)

instance Monoid Nat where
    mempty = Zero
    mappend = (+.)

instance Monoid Int where
    mempty = IntZero
    mappend = (.+.)

instance Group Int where
    ginv = intNeg

instance Monoid Rat where
    mempty = Rat IntZero Zero
    mappend = (%+)

instance Group Rat where
    ginv = ratNeg

instance Monoid (List a) where
    mempty = ITMOPrelude.List.Nil
    mappend = (++)
