{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Tree (
      Tree(..)
    , mapTree
    ) where

import ITMOPrelude.Primitive
import Prelude(Eq,Show,Read)

data Tree a = Nil | Branch { element :: a, left, right :: Tree a } deriving (Eq,Show,Read)

insertLeft :: a -> Tree a -> Tree a
insertLeft e Nil = Branch e Nil Nil
insertLeft e t = t { left = insertLeft e $ left t }

insertRight :: a -> Tree a -> Tree a
insertRight e Nil = Branch e Nil Nil
insertRight e t = t { right = insertRight e $ right t }

turnLeft :: Tree a -> Tree a
turnLeft (Branch e1 t1 (Branch e2 t2 t3)) = Branch e2 (Branch e1 t1 t2) t3
turnLeft x = x

turnRight :: Tree a -> Tree a
turnRight (Branch e1 (Branch e2 t1 t2) t3) = Branch e2 t1 (Branch e1 t2 t3)
turnRight x = x

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f (Branch e l r) = Branch (f e) (mapTree f l) (mapTree f r)
mapTree _ _ = Nil

foldrTree :: (a -> b -> b) -> b -> Tree a -> b
foldrTree op z (Branch e l r) = foldrTree op (op e rightFolded) l where
    rightFolded = foldrTree op z r
foldrTree _ z _ = z
