{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Primitive (
      undefined
    , id
    , Unit(..)
    , Pair(..)
    , Either(..)
    , Maybe(..)
    , Bool(..)
    , if'
    , Tri(..)
    , not, (&&), (||)
    , Nat(..)
    , natCmp, natEq, natLt
    , (+.), (-.), (*.)
    , natDivMod, natDiv, natMod
    , gcd
    , Sign(..)
    , PositiveNat(..)
    , Int(..)
    , intZero, intOne, intNegOne
    , intNeg
    , intCmp, intEq, intLt
    , (.+.), (.-.), (.*.)
    , Rat(..)
    , ratNeg, ratInv
    , ratCmp, ratEq, ratLt
    , (%+), (%-), (%*), (%/)
    , (.)
    , ($)
    ) where

-- Eq class added exclusivly to make code
-- easier to test
import Prelude (Eq,Show,Read)

---------------------------------------------
-- Синтаксис лямбда-выражений

-- Эквивалентные определения
example1 x  = x
example1'   = \x -> x
example1''  = let y = \x -> x in y
example1''' = y where
    y = \x -> x

-- Снова эквивалентные определения
example2 x y  = x %+ y
example2' x   = \y -> x %+ y
example2''    = \x -> \y -> x %+ y
example2'''   = \x y -> x %+ y
example2''''  = let z = \x y -> x %+ y in z
example2''''' = z where
    z x = \y -> x %+ y

-- Зацикленное выражение
undefined = undefined

-- Ниже следует реализовать все термы, состоящие из undefined заглушки.
-- Любые термы можно переписывать (natEq и natLt --- хорошие кандидаты).

-------------------------------------------
-- Примитивные типы

-- Тип с единственным элементом
data Unit = Unit deriving (Show, Read)

-- Пара, произведение
data Pair a b = Pair { fst :: a, snd :: b } deriving (Eq,Show,Read)

-- Вариант, копроизведение
data Either a b = Left a | Right b deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit a
data Maybe a = Nothing | Just a deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit Unit
data Bool = False | True deriving (Eq,Show,Read)

-- Следует отметить, что встроенный if с этим Bool использовать нельзя,
-- зато case всегда работает.

-- Ну или можно реализовать свой if
if' True a b = a
if' False a b = b

-- Трихотомия. Замечательный тип, показывающий результат сравнения
data Tri = LT | EQ | GT deriving (Eq,Show,Read)


id :: a -> a
id x = x

-------------------------------------------
-- Булевы значения

-- Логическое "НЕ"
not :: Bool -> Bool
not True = False
not False = True

infixr 3 &&
-- Логическое "И"
(&&) :: Bool -> Bool -> Bool
True  && True = True
_ && _ = False

infixr 2 ||
-- Логическое "ИЛИ"
(||) :: Bool -> Bool -> Bool
False || False = False
_ || _ = True

-------------------------------------------
-- Натуральные числа

data Nat = Zero | Succ Nat deriving (Eq,Show,Read)

natZero = Zero     -- 0
natOne = Succ Zero -- 1

-- Сравнивает два натуральных числа
natCmp :: Nat -> Nat -> Tri
natCmp Zero Zero = EQ
natCmp Zero _ = LT
natCmp _ Zero = GT
natCmp (Succ m) (Succ n) = natCmp m n

-- n совпадает с m 
natEq :: Nat -> Nat -> Bool
natEq n m = case natCmp n m of
    EQ -> True
    _  -> False

-- n меньше m
natLt :: Nat -> Nat -> Bool
natLt n m = case natCmp n m of
    LT -> True
    _  -> False

infixl 6 +.
-- Сложение для натуральных чисел
(+.) :: Nat -> Nat -> Nat
Zero     +. m = m
(Succ n) +. m = Succ (n +. m)

infixl 6 -.
-- Вычитание для натуральных чисел
(-.) :: Nat -> Nat -> Nat
n -. Zero = n
Zero -. _ = Zero
(Succ n) -. (Succ m) = n -. m

infixl 7 *.
-- Умножение для натуральных чисел
(*.) :: Nat -> Nat -> Nat
Zero     *. m = Zero
(Succ n) *. m = m +. (n *. m)

-- Целое и остаток от деления n на m
-- Comment:
-- Ноль делить на ноль даст Pair Zero Zero
natDivMod :: Nat -> Nat -> Pair Nat Nat
natDivMod n m = case natLt n m of
    True  -> Pair Zero n
    False -> Pair (Succ $ fst p) (snd p)
        where p = natDivMod (n -. m) m

natDiv n = fst . natDivMod n -- Целое
natMod n = snd . natDivMod n -- Остаток

-- Поиск GCD алгоритмом Евклида (должен занимать 2 (вычислителельная часть) + 1 (тип) строчки)
gcd :: Nat -> Nat -> Nat
gcd n Zero = n
gcd n m = gcd m (natMod n m)

-------------------------------------------
-- Целые числа

type NonNegativeNat = Nat

data PositiveNat = POne | PSucc PositiveNat deriving (Eq,Show,Read)

type PNat = PositiveNat

convertPNatToNat :: PNat -> Nat
convertPNatToNat POne = Succ Zero
convertPNatToNat (PSucc n) = Succ $ convertPNatToNat n

data Sign = Plus | Minus deriving (Eq,Show,Read)

invertSign :: Sign -> Sign
invertSign Plus = Minus
invertSign Minus = Plus

-- Требуется, чтобы представление каждого числа было единственным
data Int = IntZero | Int Sign PNat deriving (Eq,Show,Read)

intZero   = IntZero       -- 0
intOne    = Int Plus POne  -- 1
intNegOne = Int Minus POne -- -1

convertNatToInt :: Nat -> Int
convertNatToInt Zero = IntZero
convertNatToInt (Succ Zero) = Int Plus POne
convertNatToInt (Succ n) = Int Plus (PSucc m)
    where Int _ m = convertNatToInt n

-- n -> - n
intNeg :: Int -> Int
intNeg IntZero = IntZero
intNeg (Int s n) = Int (invertSign s) n

intSignCmp :: Int -> Int -> Tri
intSignCmp (Int s1 _) (Int s2 _) = case (s1, s2) of
    (Minus, Plus) -> LT
    (Plus, Minus) -> GT
    (_, _) -> EQ
intSignCmp (Int s _) _ = case s of
    Plus  -> GT
    Minus -> LT
intSignCmp _ (Int s _) = case s of
    Plus  -> LT
    Minus -> GT
intSignCmp _ _ = EQ

pNatCmp :: PNat -> PNat -> Tri
pNatCmp n m = natCmp (convertPNatToNat n) (convertPNatToNat m)

invTri :: Tri -> Tri
invTri LT = GT
invTri EQ = EQ
invTri GT = LT

-- Дальше также как для натуральных
intCmp :: Int -> Int -> Tri
intCmp n m = case (intSignCmp n m, n, m) of
    (EQ, Int sn n, Int _ m) ->
        case sn of Plus -> res
                   Minus -> invTri res
        where res = pNatCmp n m
    (res, _, _) -> res

intEq :: Int -> Int -> Bool
intEq n m = case intCmp n m of
    EQ -> True
    _  -> False

intLt :: Int -> Int -> Bool
intLt n m = case intCmp n m of
    LT -> True
    _  -> False

intAbsToNat :: Int -> Nat
intAbsToNat IntZero = Zero
intAbsToNat (Int s n) = convertPNatToNat n

intCopySign :: Int -> Int -> Int
intCopySign (Int sn _) (Int _ m) = Int sn m
intCopySign IntZero (Int _ n) = Int Plus n
intCopySign _ n = n

infixl 6 .+., .-.

(.+.) :: Int -> Int -> Int
n .+. m = case intSignCmp n m of
    EQ -> fromNat n $ absN +. absM
    _ -> if' (natLt absN absM)
             (fromNat m $ absM -. absN)
             (fromNat n $ absN -. absM) 
  where
    absN = intAbsToNat n
    absM = intAbsToNat m
    fromNat sign modulo = intCopySign sign $ convertNatToInt modulo

(.-.) :: Int -> Int -> Int
n .-. m = n .+. intNeg m

intSetSign :: Sign -> Int -> Int
intSetSign sign = intCopySign (Int sign POne)

infixl 7 .*.
(.*.) :: Int -> Int -> Int
n .*. m = let modulo = convertNatToInt $ intAbsToNat n *. intAbsToNat m in
    case intSignCmp n m of EQ -> intSetSign Plus modulo
                           _  -> intSetSign Minus modulo

-------------------------------------------
-- Рациональные числа

data Rat = Rat Int Nat deriving (Show)

ratNeg :: Rat -> Rat
ratNeg (Rat x y) = Rat (intNeg x) y

-- У рациональных ещё есть обратные элементы
ratInv :: Rat -> Rat
ratInv (Rat x y) = Rat (intCopySign x (convertNatToInt y)) (intAbsToNat x)

-- Дальше как обычно
ratCmp :: Rat -> Rat -> Tri
ratCmp (Rat x1 y1) (Rat x2 y2) = intCmp (x1 .*. convertNatToInt y2) (x2 .*. convertNatToInt y1)

ratEq :: Rat -> Rat -> Bool
ratEq l r = case ratCmp l r of
    EQ -> True
    _  -> False

ratLt :: Rat -> Rat -> Bool
ratLt l r = case ratCmp l r of
    LT -> True
    _  -> False

infixl 7 %+, %-
(%+) :: Rat -> Rat -> Rat
(Rat x1 y1) %+ (Rat x2 y2) = Rat (x1 .*. convertNatToInt y2 .+. x2 .*. convertNatToInt y1) (y1 *. y2)

(%-) :: Rat -> Rat -> Rat
n %- m = n %+ ratNeg m

infixl 7 %*, %/
(%*) :: Rat -> Rat -> Rat
(Rat x1 y1) %* (Rat x2 y2) = Rat (x1 .*. x2) (y1 *. y2)

(%/) :: Rat -> Rat -> Rat
n %/ m = n %* ratInv m

-- TODO: упс. Получились не рациональные числа, а непонятно что.
-- На ноль делить всё-таки не стоит

-------------------------------------------
-- Операции над функциями.
-- Определены здесь, но использовать можно и выше

infixr 9 .
f . g = \ x -> f (g x)

infixr 0 $
f $ x = f x

-- Эквивалентные определения
example3   a b c = gcd a (gcd b c)
example3'  a b c = gcd a $ gcd b c
example3'' a b c = ($) (gcd a) (gcd b c)

-- И ещё эквивалентные определения
example4  a b x = (gcd a (gcd b x))
example4' a b = gcd a . gcd b
