{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.List (
      List(..)
    , length
    , (++)
    , tail
    , init
    , head
    , last
    , take
    , drop
    , filter
    , gfilter
    , takeWhile
    , dropWhile
    , span
    , break
    , (!!)
    , reverse
    , subsequences
    , permutations, permutations'
    , repeat
    , foldl
    , scanl
    , foldr
    , scanr
    , map
    , concat
    , concatMap
    , zip
    , zipWith
    ) where

import Prelude (Show,Read,error)
import qualified Prelude
import ITMOPrelude.Primitive

---------------------------------------------
-- Что надо делать?
--
-- Все undefined превратить в требуемые термы.
-- Звёздочкой (*) отмечены места, в которых может потребоваться думать.

---------------------------------------------
-- Определение

data List a = Nil | Cons a (List a) deriving (Show,Read)

---------------------------------------------
-- Операции

-- Длина списка
length :: List a -> Nat
length Nil = Zero
length (Cons _ xs) = Succ $ length xs

-- Склеить два списка за O(length a)
(++) :: List a -> List a -> List a
Nil ++ b = b
Cons x xs ++ b = Cons x (xs ++ b)

-- Список без первого элемента
tail :: List a -> List a
tail Nil = errorIn "tail" "empty list"
tail (Cons _ xs) = xs

-- Список без последнего элемента
init :: List a -> List a
init Nil = errorIn "init" "empty list"
init (Cons _ Nil) = Nil
init (Cons x xs) = Cons x $ init xs

-- Первый элемент
head :: List a -> a
head Nil = errorIn "head" "empty list"
head (Cons x _) = x

-- Последний элемент
last :: List a -> a
last Nil = errorIn "last" "empty list"
last (Cons x Nil) = x
last (Cons _ xs) = last xs

-- n первых элементов списка
take :: Nat -> List a -> List a
take (Succ n) (Cons x xs) = Cons x $ take n xs
take _ _ = Nil

-- Список без n первых элементов
drop :: Nat -> List a -> List a
drop (Succ n) (Cons _ xs) = drop n xs 
drop _ l = l

-- Оставить в списке только элементы удовлетворяющие p
filter :: (a -> Bool) -> List a -> List a
filter p = gfilter (\x -> if' (p x) (Just x) Nothing)

-- Обобщённая версия. Вместо "выбросить/оставить" p
-- говорит "выбросить/оставить b".
gfilter :: (a -> Maybe b) -> List a -> List b
gfilter p Nil = Nil
gfilter p (Cons a xs) = let rest = gfilter p xs in
    case p a of Nothing -> rest
                Just b  -> Cons b rest

-- Копировать из списка в результат до первого нарушения предиката
-- takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
takeWhile :: (a -> Bool) -> List a -> List a
takeWhile p Nil = Nil
takeWhile p (Cons x xs) = if' (p x) (Cons x $ takeWhile p xs) Nil

-- Не копировать из списка в результат до первого нарушения предиката,
-- после чего скопировать все элементы, включая первый нарушивший
-- dropWhile (< 3) [1,2,3,4,1,2,3,4] == [3,4,1,2,3,4]
dropWhile :: (a -> Bool) -> List a -> List a
dropWhile p Nil = Nil
dropWhile p (Cons x xs) = if' (p x) (dropWhile p xs) xs

-- Разбить список по предикату на (takeWhile p xs, dropWhile p xs),
-- но эффективнее
span :: (a -> Bool) -> List a -> Pair (List a) (List a)
span p Nil = Pair Nil Nil
span p (Cons x xs) = Pair before after where
    pRes   = p x
    rest   = span p xs
    before = if' pRes (fst rest) Nil
    after  = if' pRes (snd rest) xs

-- Разбить список по предикату на (takeWhile (not . p) xs, dropWhile (not . p) xs),
-- но эффективнее
break :: (a -> Bool) -> List a -> Pair (List a) (List a)
break p = span (not . p)

-- n-ый элемент списка (считая с нуля)
(!!) :: List a -> Nat -> a
Nil !! n = errorIn "(!!)" "index too large"
Cons x _ !! Zero = x
Cons _ xs !! Succ n = xs !! n

-- Список задом на перёд
reverse :: List a -> List a
reverse l = reverse_append l Nil where
    reverse_append :: List a -> List a -> List a
    reverse_append Nil to = to
    reverse_append (Cons a fromTail) to = reverse_append fromTail $ Cons a to

-- (*) Все подсписки данного списка
subsequences :: List a -> List (List a)
subsequences Nil = Cons Nil Nil
subsequences (Cons x xs) = (map (Cons x) rest) ++ rest where
    rest = subsequences xs

split :: Nat -> List a -> Pair (List a) (List a)
split n l = Pair (take n l) (drop n l)

rangeOfLength :: List a -> List Nat
rangeOfLength = init . (scanl (\x y -> Succ x) Zero)

-- (*) Все перестановки элементов данного списка
-- P(s) = {x ++ P(s / x) | x <- s}
permutations :: List a -> List (List a)
permutations (Cons x Nil) = Cons (Cons x Nil) Nil 
permutations l = concatMap gen (rangeOfLength l) where
--    gen :: Nat -> List (List b)
    gen n = map (Cons x) (permutations (before ++ after)) where
        Pair before xafter = split n l
        x = head xafter
        after = tail xafter

-- (*) Если можете. Все перестановки элементов данного списка
-- другим способом
-- P(x:xs) = {insert n P(xs) | n <- len(xs)}
permutations' :: List a -> List (List a)
permutations' (Cons x Nil) = Cons (Cons x Nil) Nil
permutations' list@(Cons x xs) = concatMap gen (permutations' xs) where
    insert :: Nat -> a -> List a -> List a
    insert n e l = before ++ (Cons e Nil) ++ after where
        Pair before after = split n l

    indexes = rangeOfLength list

    --gen :: List a -> List (List a)
    gen l = map (\n -> insert n x l) indexes

-- Повторяет элемент бесконечное число раз
repeat :: a -> List a
repeat a = Cons a (repeat a)

-- Левая свёртка
-- порождает такое дерево вычислений:
--         f
--        / \
--       f   ...
--      / \
--     f   l!!2
--    / \
--   f   l!!1
--  / \
-- z  l!!0
foldl :: (a -> b -> a) -> a -> List b -> a
foldl f z Nil = z
foldl f z (Cons x xs) = foldl f (f z x) xs

-- Тот же foldl, но в списке оказываются все промежуточные результаты
-- last (scanl f z xs) == foldl f z xs
scanl :: (a -> b -> a) -> a -> List b -> List a
scanl f z Nil = Cons z Nil
scanl f z (Cons x xs) = Cons z $ scanl f (f z x) xs

-- Правая свёртка
-- порождает такое дерево вычислений:
--    f
--   /  \
-- l!!0  f
--     /  \
--   l!!1  f
--       /  \
--    l!!2  ...
--           \
--            z
--            
foldr :: (a -> b -> b) -> b -> List a -> b
foldr f z Nil = z
foldr f z (Cons x xs) = f x (foldr f z xs)

-- Аналогично
--  head (scanr f z xs) == foldr f z xs.
scanr :: (a -> b -> b) -> b -> List a -> List b
scanr f z Nil = Cons z Nil
scanr f z (Cons x xs) = Cons (f x $ head listRest) listRest where
    listRest = scanr f z xs

-- Должно завершаться за конечное время
finiteTimeTest = take (Succ $ Succ $ Succ $ Succ Zero) $ foldr (Cons) Nil $ repeat Zero

-- Применяет f к каждому элементу списка
map :: (a -> b) -> List a -> List b
map f Nil = Nil
map f (Cons x xs) = Cons (f x) (map f xs)

-- Склеивает список списков в список
concat :: List (List a) -> List a
concat = foldr (++) Nil

-- Эквивалент (concat . map), но эффективнее
concatMap :: (a -> List b) -> List a -> List b
concatMap f = foldr ((++) . f) Nil

-- Сплющить два списка в список пар длинны min (length a, length b)
zip :: List a -> List b -> List (Pair a b)
zip = zipWith Pair

-- Аналогично, но плющить при помощи функции, а не конструктором Pair
zipWith :: (a -> b -> c) -> List a -> List b -> List c
zipWith f (Cons x xs) (Cons y ys) = Cons (f x y) (zipWith f xs ys)
zipWith _ _ _ = Nil

-- Error reporting
module_str = "List"
errorIn function what = error (module_str ++ "." ++ function ++ ": " ++ what) where
    (++) = (Prelude.++)
