{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.IO where

import ITMOPrelude.Primitive
import ITMOPrelude.List
import ITMOPrelude.Categories

data RealWorld = RealWorld
    { stdIn :: List Nat
    , stdOut :: List Nat
    , exitCode :: Nat }

type IO a = State RealWorld a

getNat :: IO Nat
getNat = State (\world ->
    (world { stdIn = tail $ stdIn world}, head $ stdIn world))

putNat :: Nat -> IO ()
putNat n = State $ \world -> (world { stdIn = n `Cons` stdIn world }, ())

setExitCode :: Nat -> IO ()
setExitCode e = State $ \world -> (world { exitCode = e }, ())
