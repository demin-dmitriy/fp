import Test.Tasty

import qualified ITMOPrelude.PrimitiveTest as PT

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
    [ testGroup "Primitive" PT.tests
    ]
