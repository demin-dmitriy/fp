module ITMOPrelude.PrimitiveTest (tests) where

import qualified ITMOPrelude.Primitive as P
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Control.Monad
import Data.Ratio

instance Arbitrary P.Bool where
    arbitrary = oneof [return P.True, return P.False]

instance Arbitrary P.Sign where
    arbitrary = oneof [return P.Plus, return P.Minus]

instance Arbitrary P.Int where
    arbitrary = liftM (toMyInt . getSmall) arbitrary 

instance Arbitrary P.Nat where
    arbitrary = liftM (toMyNat . getNonNegative) arbitrary

instance Arbitrary P.Rat where
    arbitrary = liftM2 P.Rat arbitrary arbitrary

toMyNat :: Int -> P.Nat
toMyNat n | n >= 0 = iterate P.Succ P.Zero !! n 

fromMyNat :: P.Nat -> Int
fromMyNat n = addIntToNat 0 n where
    addIntToNat k P.Zero = k
    addIntToNat k (P.Succ n) = addIntToNat (k + 1) n

toMyInt :: Int -> P.Int
toMyInt 0 = P.IntZero
toMyInt n = P.Int sign (iterate P.PSucc P.POne !! (abs n - 1)) where
    sign | n > 0 = P.Plus
         | n < 0 = P.Minus

fromMyInt :: P.Int -> Int
fromMyInt P.IntZero = 0
fromMyInt (P.Int sign pnat) = sign_value * (add_pnat 0 pnat) where
    sign_value = case sign of
        P.Plus -> 1
        P.Minus -> -1
    add_pnat :: Int -> P.PositiveNat -> Int
    add_pnat k P.POne = k + 1
    add_pnat k (P.PSucc n) = add_pnat (k + 1) n

fromMyRat :: P.Rat -> Ratio Int
fromMyRat (P.Rat d n) = fromMyInt d % fromMyNat n

compareInt :: Int -> Int -> P.Tri
compareInt n m | n >  m = P.GT
               | n == m = P.EQ
               | n <  m = P.LT

fromMyBool :: P.Bool -> Bool
fromMyBool P.True = True
fromMyBool P.False = False

ignore = const $ testGroup "Ignored tests" []

tests =
    [ testGroup "UtilityFunctions"
        [ testProperty "`not` is involutive" $
            \b -> P.not (P.not b) == b
        ]
    , testGroup "Integer"
        -- QuickCheck testing is not enough because generator for P.Int uses
        -- toMyInt
        [ testProperty "`toMyInt` is left inverse of `fromMyInt`" $
            \i -> (toMyInt . fromMyInt) i == i
        , testProperty "`toMyInt` is right inverse of `fromMyInt`" $
            \i -> let i' = getSmall i in (fromMyInt . toMyInt) i' == i'
        , testCase "toMyInt on simple values" $
            map toMyInt [-1, 0, 1] @?= [P.intNegOne, P.intZero, P.intOne]
        , testCase "fromMyInt . toMyInt = id on simple values" $
            let list = [-20 .. 20] in map (fromMyInt . toMyInt) list @?= list
        , testProperty "Int negation" $
            \i -> fromMyInt (P.intNeg i) == -(fromMyInt i)
        , testProperty "intCmp" $
            \i j -> P.intCmp i j == compareInt (fromMyInt i) (fromMyInt j)
        , testProperty "intEq" $
            \i j -> fromMyBool (P.intEq i j) == (fromMyInt i == fromMyInt j)
        , testProperty "intLt" $
            \i j -> fromMyBool (P.intLt i j) == (fromMyInt i < fromMyInt j)
        , testProperty "(.+.)" $
            \i j -> fromMyInt i + fromMyInt j == fromMyInt (i P..+. j)
        , testProperty "(.-.)" $
            \i j -> fromMyInt i - fromMyInt j == fromMyInt (i P..-. j)
        , testProperty "(.*.)" $
            \i j -> fromMyInt i * fromMyInt j == fromMyInt (i P..*. j)
        ]
    , testGroup "Nat"
        [ testCase "fromMyNat . toMyNat on simple values" $
            let list = [0 .. 50] in map (fromMyNat . toMyNat) list @?= list
        , testCase "toMyNat 0 is Zero" $
            toMyNat 0 @?= P.Zero
        ]
    , ignore $ testGroup "Rational"
        [ testProperty "ratNeg" $
            \r -> fromMyRat (P.ratNeg r) == -(fromMyRat r)
        -- TODO

        ]
    ]
